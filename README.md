# README #

Simple program that creates a number spiral from 1 to the Square of the number entered
e.g.

Input: 5

Output:

    [1, 2, 3, 4, 5]

    [16, 17, 18, 19, 6]

    [15, 24, 25, 20, 7]

    [14, 23, 22, 21, 8]

    [13, 12, 11, 10, 9]