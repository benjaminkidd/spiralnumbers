package me.benjaminkayeight.spiralnumbers;
/*
    Authors: leynaa and BenjaminKayEight
 */
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    private static int spiral [][];
    private static int size;

    public static void main(String[] args) {
	    cli();
    }

    public static void cli(){

        System.out.println("Please enter a number to spiral:");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        genSpiral(n);
        printArray();
    }
    //print generated 2D array
    private static void printArray(){
        for (int[] row : spiral){
            System.out.println(Arrays.toString(row));
        }
    }

    //Generate spiral of numbers from 1 to n^2
    private static void genSpiral(int n){
        size = n*n;
        spiral = new int[n][n];
        int count = 1;
        int x=0,y=0,loop=1;
        while( count < size){
            //go right
            while(x< n-loop){ //0-3
                spiral[y][x]=count;
                System.out.println(count);
                count++;
                x++;
            }
            //go down
            while(y< n-loop){
                spiral[y][x]=count;
                count++;
                y++;
            }
            //go left
            while(x>= loop){
                spiral[y][x]=count;
                count++;
                x--;
            }
            //go up
            while(y> loop){
                spiral[y][x]=count;
                count++;
                y--;
            }
            loop++;
        }
        //does the last element
        if(count == size){
            spiral[y][x]=count;
        }
    }
}
